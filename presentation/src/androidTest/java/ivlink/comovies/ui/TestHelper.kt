package ivlink.comovies.ui

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.StateListDrawable
import android.view.View
import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import ivlink.comovies.ui.overview.models.MovieOverviewViewHolder
import org.hamcrest.Description
import org.hamcrest.Matcher


// Created by IvLink on 3/27/19.
class TestHelper<T : Activity>(private val activityTestRule: ActivityTestRule<T>) {

    fun checkAccessibilityItems() {
        if (getListCount() > 0) {
            Espresso.onView(ViewMatchers.withId(ivlink.comovies.R.id.rvMoviesList))
                .perform(RecyclerViewActions.actionOnItemAtPosition<MovieOverviewViewHolder>(0, ViewActions.click()))
            Espresso.onView(ViewMatchers.withId(ivlink.comovies.R.id.toolbar)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }
    }

    fun getListCount(): Int {
        val recyclerView = activityTestRule.activity.findViewById(ivlink.comovies.R.id.rvMoviesList) as RecyclerView
        return recyclerView.adapter!!.itemCount
    }

    fun withCustomConstraints(action: ViewAction, constraints: Matcher<View>): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return constraints
            }

            override fun getDescription(): String {
                return action.description
            }

            override fun perform(uiController: UiController, view: View) {
                action.perform(uiController, view)
            }
        }
    }

    fun withImageDrawable(resourceId: Int): Matcher<View> {
        return object : BoundedMatcher<View, ImageView>(ImageView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has image drawable resource $resourceId")
            }

            public override fun matchesSafely(imageView: ImageView): Boolean {
                return sameBitmap(imageView.context, imageView.drawable, resourceId)
            }
        }
    }

    private fun sameBitmap(context: Context, drawableIn: Drawable?, resourceId: Int): Boolean {
        var drawable = drawableIn
        var otherDrawable = context.resources.getDrawable(resourceId)
        if (drawable == null || otherDrawable == null) {
            return false
        }
        if (drawable is StateListDrawable && otherDrawable is StateListDrawable) {
            drawable = drawable.current
            otherDrawable = otherDrawable.current
        }

        if (drawable is BitmapDrawable) {
            val bitmap = drawable.bitmap
            val otherBitmap: Bitmap? = (otherDrawable as Drawable).current.toBitmap()
            return bitmap.sameAs(otherBitmap)
        }
        return false
    }
}