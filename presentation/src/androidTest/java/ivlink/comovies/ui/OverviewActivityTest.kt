package ivlink.comovies.ui


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import ivlink.comovies.R
import ivlink.comovies.ui.overview.models.MovieOverviewViewHolder
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class OverviewActivityTest : BaseMainActivityTest() {

    @Test
    fun overviewActivityTest() {
        onView(withId(ivlink.comovies.R.id.rvMoviesList)).check(matches(isDisplayed()))
        testHelper.checkAccessibilityItems()
    }

    @Test
    fun refreshList() {
        onView(withId(ivlink.comovies.R.id.swipeRefreshLayout)).perform(
            testHelper.withCustomConstraints(
                ViewActions.swipeDown(),
                isDisplayingAtLeast(85)
            )
        )
        Assert.assertThat(testHelper.getListCount(), Matchers.equalTo(20))
    }

    @Test
    fun loadMore() {
        onView(withId(R.id.rvMoviesList)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.rvMoviesList)).perform(
            RecyclerViewActions.scrollToPosition<MovieOverviewViewHolder>(testHelper.getListCount() - 1)
        )
        Assert.assertThat(testHelper.getListCount(), Matchers.greaterThan(20))
    }

}
