package ivlink.comovies.ui


import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import ivlink.comovies.R
import org.hamcrest.Matchers.not
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class DetailsActivityTest :BaseMainActivityTest(){


    @Test
    fun detailsActivityTest() {
        onView(withId(R.id.rvMoviesList)).check(matches(isDisplayed()))
        testHelper.checkAccessibilityItems()

        onView(withId(R.id.ivPoster)).check(matches(not(testHelper.withImageDrawable(R.mipmap.ic_launcher))))
        onView(withId(R.id.tvTitle)).check(matches(not(withText("Back SAMPLE"))))
        onView(withId(R.id.tvReleaseDate)).check(matches(not(withText("8 Nov 00"))))

        Assert.assertEquals(
            activityTestRule.activity.findViewById<TextView>(R.id.tvTitle).text,
            activityTestRule.activity.findViewById<Toolbar>(R.id.toolbar).title
        )
    }

}
