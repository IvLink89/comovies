package ivlink.comovies.ui

import androidx.test.espresso.IdlingRegistry
import androidx.test.rule.ActivityTestRule
import ivlink.comovies.idle.SimpleIdlingResource
import org.junit.Before
import org.junit.Rule

// Created by IvLink on 3/31/19.

open class BaseMainActivityTest{

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)
    open lateinit var testHelper: TestHelper<MainActivity>
    private var idlingResource = SimpleIdlingResource.getIdlingResource()

    @Before
    fun setupData() {
        IdlingRegistry.getInstance().register(idlingResource)
        testHelper = TestHelper(activityTestRule)
    }
}