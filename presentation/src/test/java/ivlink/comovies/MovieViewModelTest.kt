package ivlink.comovies

import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.*
import ivlink.comovies.ui.overview.MovieOverviewViewModel
import ivlink.data.mapper.mapToEntity
import ivlink.data.mapper.mapToRemote
import ivlink.data.repository.LocalData
import ivlink.data.repository.RemoteData
import ivlink.domain.common.ResultState
import ivlink.domain.models.MovieModel
import ivlink.domain.overview.MovieOverviewRepository
import ivlink.domain.overview.MovieOverviewUseCase
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.util.*

val MOVIE_1 = MovieModel(
    1,
    "Movie1",
    5F,
    "None",
    "None2",
    false,
    "Something about movie",
    Calendar.getInstance()
)
val MOVIE_2 = MovieModel(
    2,
    "Movie2",
    10F,
    "",
    "Empty",
    true,
    "Not for you",
    Calendar.getInstance()
)
val movies = listOf(MOVIE_1, MOVIE_2)

// Created by IvLink on 3/27/19.
class MovieViewModelTest {

    private fun <T> List<T>.toDeferredAsync() = CompletableDeferred(this)

    @Test
    @Suppress("DeferredResultUnused")
    fun checkRemoteInfo() {
        val mock = mock<RemoteData> {
            on {
                getMoviesAsync("1")
            } doReturn movies.map {
                it.mapToRemote()
            }.toDeferredAsync()
        }
        mock.getMoviesAsync("1")
        verify(mock).getMoviesAsync("1")
    }

    @Test
    fun checkDatabaseInfo() {
        val mock = mock<LocalData> {
            onBlocking {
                getMovies()
            } doReturn movies.map {
                it.mapToEntity()
            }
        }
        runBlocking {
            mock.getMovies()
        }
        verifyBlocking(mock) {
            getMovies()
        }
    }

    @Test
    fun loadFromRepoAndLoadToViewModel() {
        val repository = mock<MovieOverviewRepository>()
        val viewModel = mock<MovieOverviewViewModel>()
        val useCase = mock<MovieOverviewUseCase>()

        runBlocking {
            whenever(repository.getMovies("1"))
                .thenReturn(MutableLiveData(ResultState.Success(movies)))//TODO android package in unit test
        }
        runBlocking {
            whenever(useCase.getMovies("1")).then {
                repository
            }
        }
        whenever(viewModel.getMovies("1")).then {
            useCase
        }
        viewModel.getMovies("1")
        verifyBlocking(viewModel) {
            getMovies("1")
        }
    }

    @Test
    fun loadFromRepoAndGetException() {
        val repository = mock<MovieOverviewRepository>()
        runBlocking {
            whenever(repository.getMovies("1"))
                .thenReturn(MutableLiveData(ResultState.Error(IllegalStateException("Test Illegal"))))//TODO android package in unit test
        }
        val useCase = MovieOverviewUseCase(repository)
        val viewModel = MovieOverviewViewModel(useCase)

        viewModel.getMovies("1")
        viewModel.getMovies("1")
        viewModel.getMovies("1")

        verifyBlocking(repository, times(3)) {
            getMovies("1")
        }
    }
}