package ivlink.comovies.data

import ivlink.data.utils.Converters
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.Calendar
import java.util.Calendar.DAY_OF_MONTH
import java.util.Calendar.MONTH
import java.util.Calendar.SEPTEMBER
import java.util.Calendar.YEAR

class ConvertersTest {

    private val cal = Calendar.getInstance().apply {
        set(YEAR, 2019)
        set(MONTH, SEPTEMBER)
        set(DAY_OF_MONTH, 19)
    }

    @Test fun calendarToDateStamp() {
        assertEquals(cal.timeInMillis, Converters().calendarToDatestamp(cal))
    }

    @Test fun dateStampToCalendar() {
        assertEquals(Converters().datestampToCalendar(cal.timeInMillis), cal)
    }
}