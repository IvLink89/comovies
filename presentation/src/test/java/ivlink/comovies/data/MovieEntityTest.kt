package ivlink.comovies.data

import ivlink.data.db.MovieEntity
import ivlink.data.mapper.mapToModel
import ivlink.domain.models.MovieModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

// Created by IvLink on 3/23/19.
class MovieEntityTest {

    private lateinit var movie: MovieEntity

    @Before
    fun setUp(){
        movie = MovieEntity(
                5,
                "Movie",
                4.3F,
                "",
                "",
                false,
                "",
                Calendar.getInstance()
        )
    }

    @Test
    fun test_values() {
        Assert.assertEquals(5, movie.id)
        Assert.assertEquals("", movie.backdropPath)
    }

    @Test
    fun test_mapping_to_movie_model() {
        val model = MovieModel(
                5,
                "Movie",
                4.3F,
                "",
                "",
                false,
                "",
                Calendar.getInstance()
        )
        Assert.assertEquals(model.id, movie.mapToModel().id)
        Assert.assertEquals(model.title, movie.mapToModel().title)
        Assert.assertEquals(model.voteAverage, movie.mapToModel().voteAverage)
        Assert.assertEquals(model.posterPath, movie.mapToModel().posterPath)
        Assert.assertEquals(model.backgroundPath, movie.mapToModel().backgroundPath)
        Assert.assertEquals(model.adult, movie.mapToModel().adult)
        Assert.assertEquals(model.overview, movie.mapToModel().overview)
        Assert.assertEquals(model.releaseDate.weekYear, movie.mapToModel().releaseDate.weekYear)

    }
}