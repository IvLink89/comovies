package ivlink.comovies.idle

import android.annotation.SuppressLint
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import androidx.test.espresso.IdlingResource
import java.util.concurrent.atomic.AtomicBoolean

// Created by IvLink on 3/30/19.
class SimpleIdlingResource : IdlingResource {

    @Volatile
    private var mCallback: IdlingResource.ResourceCallback? = null
    private val mIsIdleNow = AtomicBoolean(true)

    companion object {
        private var idlingResource: SimpleIdlingResource? = null

        @VisibleForTesting
        @NonNull
        fun getIdlingResource(): SimpleIdlingResource {
            if (idlingResource == null) {
                idlingResource = SimpleIdlingResource()
            }
            return idlingResource as SimpleIdlingResource
        }

        @SuppressLint("VisibleForTests")
        fun setIdle(isIdle:Boolean){
            SimpleIdlingResource.getIdlingResource().setIdleState(isIdle)
        }
    }

    override fun getName(): String {
        return this.javaClass.name
    }

    override fun isIdleNow(): Boolean {
        return mIsIdleNow.get()
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        mCallback = callback
    }

    private fun setIdleState(isIdleNow: Boolean) {
        mIsIdleNow.set(isIdleNow)
        if (isIdleNow && mCallback != null) {
            mCallback!!.onTransitionToIdle()
        }
    }
}