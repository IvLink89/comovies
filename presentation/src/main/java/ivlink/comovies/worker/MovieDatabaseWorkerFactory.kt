package ivlink.comovies.worker

import android.content.Context
import androidx.work.*
import ivlink.domain.overview.MovieOverviewUseCase

// Created by IvLink on 3/23/19.
class MovieDatabaseWorkerFactory(private val overviewUseCase: MovieOverviewUseCase): WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {

        val workerClass = Class.forName(workerClassName).asSubclass(CoroutineWorker::class.java)
        val constructor = workerClass.getDeclaredConstructor(Context::class.java, WorkerParameters::class.java)
        val instance = constructor.newInstance(appContext, workerParameters)

        when (instance) {
            is MovieDatabaseWorker -> {
                instance.overviewUseCase = overviewUseCase
            }
        }
        return instance
    }
}