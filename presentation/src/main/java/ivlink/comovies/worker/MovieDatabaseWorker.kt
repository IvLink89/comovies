package ivlink.comovies.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import ivlink.data.db.MovieDatabase
import ivlink.data.mapper.mapToEntity
import ivlink.domain.common.ResultState
import ivlink.domain.overview.MovieOverviewUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope

// Created by IvLink on 3/23/19.
class MovieDatabaseWorker(
    appContext: Context,
    params: WorkerParameters
) : CoroutineWorker(appContext, params) {

    lateinit var overviewUseCase: MovieOverviewUseCase

    override val coroutineContext: CoroutineDispatcher
        get() = Dispatchers.IO

    override suspend fun doWork() = coroutineScope {
        val result = overviewUseCase.fillUpDatabase()
        when (result) {
            is ResultState.Success -> {
                val database = MovieDatabase.getInstance(applicationContext)
                database.movieDao().insertMovies(result.data.map {
                    it.mapToEntity()
                })
                Result.success()
            }
            else -> {
                Result.failure()
            }
        }
    }
}