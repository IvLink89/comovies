package ivlink.comovies.ui

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import ivlink.comovies.R
import ivlink.data.utils.getReadable
import java.util.*

// Created by IvLink on 3/21/19.

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: AppCompatImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        val options = RequestOptions().placeholder(R.drawable.poster_placeholder)
        Glide.with(view.context)
            .setDefaultRequestOptions(options)
            .load("https://image.tmdb.org/t/p/w300$imageUrl")
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    } else {
        Glide.with(view.context)
            .load(R.drawable.poster_placeholder)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

@BindingAdapter("textFromCalendar")
fun bindTextFromCalendar(view: AppCompatTextView, text: Calendar?) {
    view.text = text?.getReadable()
}
