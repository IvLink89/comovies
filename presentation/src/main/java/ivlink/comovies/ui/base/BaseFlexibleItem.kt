package ivlink.comovies.ui.base

import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.viewholders.FlexibleViewHolder

// Created by IvLink on 3/22/19.
abstract class BaseFlexibleItem<VH : FlexibleViewHolder>(id: String) : AbstractFlexibleItem<VH>() {

    protected var id: String? = id
    private var title: String? = null

    override fun equals(other: Any?): Boolean {
        if (other is BaseFlexibleItem<*>) {
            val inItem = other as BaseFlexibleItem<*>?
            return this.id.equals(inItem!!.id)
        }
        return false
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "id=$id, title=$title"
    }
}