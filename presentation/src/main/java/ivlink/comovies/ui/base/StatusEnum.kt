package ivlink.comovies.ui.base

// Created by IvLink on 3/25/19.
enum class StatusEnum {
    MORE_TO_LOAD,
    DISABLE_ENDLESS,
    NO_MORE_LOAD,
    ON_CANCEL,
    ON_ERROR
}