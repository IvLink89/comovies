package ivlink.comovies.ui.details

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ivlink.domain.details.MovieDetailsUseCase
import ivlink.domain.models.MovieModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel


class MovieDetailsViewModel(private val detailsUseCase: MovieDetailsUseCase) : ViewModel() {

    var movie = MediatorLiveData<MovieModel>()

    fun getMovie(id: Long) {
        val data = detailsUseCase.getMovie(id)
        movie.addSource(data) {
            movie.removeSource(data)
            movie.value = it
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}