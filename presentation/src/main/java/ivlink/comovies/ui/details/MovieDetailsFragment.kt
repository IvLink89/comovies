package ivlink.comovies.ui.details

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import ivlink.comovies.databinding.FragmentMovieDetailsBinding
import ivlink.comovies.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel


class MovieDetailsFragment : BaseFragment() {

    private lateinit var movieId: String
    private val viewModel: MovieDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater
            .from(context)
            .inflateTransition(android.R.transition.move)
        movieId = MovieDetailsFragmentArgs.fromBundle(arguments!!).movieId
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentMovieDetailsBinding>(
            inflater, ivlink.comovies.R.layout.fragment_movie_details, container, false
        ).apply {
            movieModel = viewModel
            lifecycleOwner = this@MovieDetailsFragment
        }

        if (activity is AppCompatActivity) {
            val toolbar = binding.toolbar
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        viewModel.getMovie(movieId.toLong())
        return binding.root
    }

}