package ivlink.comovies.ui.overview.models

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.helpers.AnimatorHelper
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import ivlink.comovies.R
import ivlink.comovies.databinding.LayoutMovieOverviewItemBinding
import ivlink.comovies.ui.base.BaseFlexibleItem
import ivlink.comovies.ui.overview.MovieOverviewFragmentDirections
import ivlink.domain.models.MovieModel


// Created by IvLink on 3/22/19.
class MoviewOverviewListModel(
    id: String,
    val model: MovieModel
) : BaseFlexibleItem<MovieOverviewViewHolder>(id) {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: MovieOverviewViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        holder?.bind(createOnClickListener(model.id, holder.ivPoster!!, holder.tvTitle!!), model)
    }

    private fun createOnClickListener(
        movieId: Long,
        imageView: AppCompatImageView,
        titleView: AppCompatTextView
    ): View.OnClickListener {
        return View.OnClickListener {
            val extras = FragmentNavigatorExtras(
                imageView to "header_image",
                titleView to "header_title"
            )
            val bundle = Bundle()
            bundle.putString("movieId",movieId.toString())
            val direction = MovieOverviewFragmentDirections
                .actionMovieOverviewFragmentToMovieDetailsFragment(movieId.toString())
            it.findNavController().navigate(direction, extras)
        }
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): MovieOverviewViewHolder {
        return MovieOverviewViewHolder(
            LayoutMovieOverviewItemBinding.inflate(
                LayoutInflater.from(view?.context), null, false
            ), adapter
        )
    }

    override fun getLayoutRes(): Int = R.layout.layout_movie_overview_item
}

class MovieOverviewViewHolder(val view: LayoutMovieOverviewItemBinding?, adapter: FlexibleAdapter<out IFlexible<*>>?) :
    FlexibleViewHolder(view?.root, adapter) {
    val ivPoster = view?.ivPoster
    val tvTitle = view?.tvTitle

    fun bind(listener: View.OnClickListener, model: MovieModel) {

        view?.apply {
            clickListener = listener
            movieModel = model
            executePendingBindings()
        }
    }

    override fun scrollAnimators(animators: MutableList<Animator>, position: Int, isForward: Boolean) {
        AnimatorHelper.alphaAnimator(animators, itemView, 1.0F)
    }
}