package ivlink.comovies.ui.overview.models

import android.animation.Animator
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.Payload
import eu.davidea.flexibleadapter.helpers.AnimatorHelper
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import ivlink.comovies.databinding.LayoutLoadMoreOverviewItemBinding
import ivlink.comovies.ui.base.BaseFlexibleItem
import ivlink.comovies.ui.base.StatusEnum
import ivlink.comovies.ui.overview.models.LoadMoreModel.Companion.status


// Created by IvLink on 3/25/19.
class LoadMoreModel(id: String) : BaseFlexibleItem<LoadMoreViewHolder>(id) {

    companion object {
        var status = StatusEnum.MORE_TO_LOAD
    }

    override fun bindViewHolder(
            adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
            holder: LoadMoreViewHolder?,
            position: Int,
            payloads: MutableList<Any>?
    ) {
        holder?.bind(payloads!!)
    }

    override fun createViewHolder(
            view: View?,
            adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): LoadMoreViewHolder {
        return LoadMoreViewHolder(
                LayoutLoadMoreOverviewItemBinding.inflate(
                        LayoutInflater.from(view?.context), null, false
                ), adapter
        )
    }

    override fun getLayoutRes(): Int = ivlink.comovies.R.layout.layout_load_more_overview_item
}

class LoadMoreViewHolder(val view: LayoutLoadMoreOverviewItemBinding?, private val adapter: FlexibleAdapter<out IFlexible<*>>?) :
        FlexibleViewHolder(view?.root, adapter) {
    private val loadMoreIndicator = view?.loadMoreIndicator

    fun bind(payloads: MutableList<Any>) {
        loadMoreIndicator?.visibility = View.GONE

        if (!adapter?.isEndlessScrollEnabled!!) {
            status = StatusEnum.DISABLE_ENDLESS
        } else if (payloads.contains(Payload.NO_MORE_LOAD)) {
            status = StatusEnum.NO_MORE_LOAD
        }

        when (status) {
            StatusEnum.NO_MORE_LOAD -> status = StatusEnum.NO_MORE_LOAD
            StatusEnum.DISABLE_ENDLESS -> { }
            StatusEnum.ON_CANCEL -> status = StatusEnum.MORE_TO_LOAD
            StatusEnum.ON_ERROR -> status = StatusEnum.MORE_TO_LOAD
            else -> loadMoreIndicator?.visibility = View.VISIBLE
        }
    }

    override fun scrollAnimators(animators: MutableList<Animator>, position: Int, isForward: Boolean) {
        AnimatorHelper.alphaAnimator(animators, itemView, 1.0F)
    }
}