package ivlink.comovies.ui.overview

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.work.*
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import ivlink.comovies.R
import ivlink.comovies.databinding.FragmentMovieOverviewBinding
import ivlink.comovies.idle.SimpleIdlingResource
import ivlink.comovies.ui.base.BaseFragment
import ivlink.comovies.ui.overview.models.LoadMoreModel
import ivlink.comovies.ui.overview.models.MoviewOverviewListModel
import ivlink.comovies.worker.MovieDatabaseWorker
import ivlink.data.utils.DATABASE_UPDATE_IN_MINUTES
import ivlink.domain.common.ResultState
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.TimeUnit

const val LIST_THRESHOLD = 4
var CURRENT_PAGE = 1

class MovieOverviewFragment : BaseFragment(), FlexibleAdapter.EndlessScrollListener {

    private val overviewViewModel: MovieOverviewViewModel by viewModel()
    private val sharedPreferences: SharedPreferences by inject()

    private val flexAdapter = FlexibleAdapter(mutableListOf())

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val request = PeriodicWorkRequestBuilder<MovieDatabaseWorker>(DATABASE_UPDATE_IN_MINUTES, TimeUnit.MINUTES)
            .setConstraints(constraints)
            .build()
        WorkManager.getInstance().enqueueUniquePeriodicWork(
            "databaseFillUp",
            ExistingPeriodicWorkPolicy.REPLACE,
            request
        )

        flexAdapter.apply {
            addListener(this)
            setAnimationOnForwardScrolling(true)
            setAnimationOnReverseScrolling(true)
            setEndlessScrollThreshold(LIST_THRESHOLD)
            setEndlessScrollListener(
                this@MovieOverviewFragment,
                LoadMoreModel("LoadMoreModelFlexAdapter")
            )
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentMovieOverviewBinding.inflate(inflater, container, false)

        binding.rvMoviesList.apply {
            this.adapter = flexAdapter
            this.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }
        binding.swipeRefreshLayout.setOnRefreshListener {
            CURRENT_PAGE = 1
            flexAdapter.clear()
            overviewViewModel.getMovies(CURRENT_PAGE.toString())
        }

        overviewViewModel.movies.observe(viewLifecycleOwner, Observer {
            when (it) {
                is ResultState.Success -> {
                    val list: MutableList<IFlexible<RecyclerView.ViewHolder>> = mutableListOf()
                    it.data.forEach { movieModel ->
                        val listModel = MoviewOverviewListModel("${UUID.randomUUID()}", movieModel)
                        list.add(listModel as IFlexible<RecyclerView.ViewHolder>)
                    }
                    binding.swipeRefreshLayout.isRefreshing = false
                    flexAdapter.onLoadMoreComplete(list)
                    SimpleIdlingResource.setIdle(true)
                }
                is ResultState.Error -> {
                    Toast.makeText(activity, it.exception.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        overviewViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it) binding.loadingProgress.visibility = View.VISIBLE
            else binding.loadingProgress.visibility = View.GONE
        })

        overviewViewModel.getMovies(CURRENT_PAGE.toString())
        return binding.root
    }

    override fun onDetach() {
        super.onDetach()
        sharedPreferences.edit().clear().apply()
    }

    override fun noMoreLoad(newItemsSize: Int) {
        Toast.makeText(activity, getString(R.string.warning_no_more_items), Toast.LENGTH_LONG).show()
    }

    override fun onLoadMore(lastPosition: Int, currentPage: Int) {
        sharedPreferences.edit {
            CURRENT_PAGE += 1
            putString("CURRENT_PAGE", CURRENT_PAGE.toString())
        }
        overviewViewModel.getMovies(sharedPreferences.getString("CURRENT_PAGE", "1")!!)
    }
}