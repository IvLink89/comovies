package ivlink.comovies.ui.overview

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ivlink.comovies.idle.SimpleIdlingResource
import ivlink.domain.common.ResultState
import ivlink.domain.models.MovieModel
import ivlink.domain.overview.MovieOverviewUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MovieOverviewViewModel(private val overviewUseCase: MovieOverviewUseCase) : ViewModel() {

    val movies: MutableLiveData<ResultState<List<MovieModel>>> = MutableLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData(true)

    fun getMovies(page: String) {
        SimpleIdlingResource.setIdle(false)
        viewModelScope.launch(Dispatchers.IO) {
            movies.postValue(overviewUseCase.getMovies(page).value)
            loading.postValue(false)
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}