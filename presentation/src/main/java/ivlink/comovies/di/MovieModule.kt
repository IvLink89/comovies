package ivlink.comovies.di


import android.app.Application
import android.content.SharedPreferences
import androidx.work.WorkerFactory
import ivlink.comovies.BuildConfig
import ivlink.comovies.ui.details.MovieDetailsViewModel
import ivlink.comovies.ui.overview.MovieOverviewViewModel
import ivlink.comovies.worker.MovieDatabaseWorkerFactory
import ivlink.data.db.LocalDataImpl
import ivlink.data.db.MovieDatabase
import ivlink.data.network.RemoteDataImpl
import ivlink.data.network.RemoteServiceFactory.makeRemoteService
import ivlink.data.repository.LocalData
import ivlink.data.repository.MovieDetailsRepoImpl
import ivlink.data.repository.MovieOverviewRepoImpl
import ivlink.data.repository.RemoteData
import ivlink.domain.details.MovieDetailsRepository
import ivlink.domain.details.MovieDetailsUseCase
import ivlink.domain.overview.MovieOverviewRepository
import ivlink.domain.overview.MovieOverviewUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

// Created by IvLink on 3/21/19.

val movieModule = module {

    single<LocalData> { LocalDataImpl(get()) }
    single<RemoteData> { RemoteDataImpl(makeRemoteService(BuildConfig.DEBUG)) }
    single<MovieOverviewRepository> { MovieOverviewRepoImpl(get(), get(), get()) }
    single { MovieOverviewUseCase(get()) }
    viewModel { MovieOverviewViewModel(get()) }
    single { MovieDatabase.getInstance(get()).movieDao() }
    single<WorkerFactory> { MovieDatabaseWorkerFactory(get()) }

    single { MovieDetailsUseCase(get()) }
    viewModel { MovieDetailsViewModel(get()) }
    single<MovieDetailsRepository> { MovieDetailsRepoImpl(get()) }

    single { getSharedPrefs(get()) }
}

fun getSharedPrefs(androidApplication: Application): SharedPreferences{
    return  androidApplication.getSharedPreferences("CoMovies",  android.content.Context.MODE_PRIVATE)
}

val movieModuleApp = listOf(movieModule)