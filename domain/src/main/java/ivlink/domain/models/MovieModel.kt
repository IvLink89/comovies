package ivlink.domain.models

import java.util.*

data class MovieModel(
        val id: Long = 0,
        val title: String,
        val voteAverage: Float = 0F,
        val posterPath: String?,
        val backgroundPath: String?,
        val adult: Boolean,
        val overview: String?,
        val releaseDate: Calendar
)