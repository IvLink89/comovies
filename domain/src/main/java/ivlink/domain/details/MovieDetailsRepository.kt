package ivlink.domain.details

import androidx.lifecycle.LiveData
import ivlink.domain.models.MovieModel

interface MovieDetailsRepository{
    fun getMovie(id : Long): LiveData<MovieModel>
}