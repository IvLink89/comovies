package ivlink.domain.details

import androidx.lifecycle.LiveData
import ivlink.domain.models.MovieModel

// Created by IvLink on 3/20/19.

class MovieDetailsUseCase(val repository: MovieDetailsRepository) {

    fun getMovie(id: Long): LiveData<MovieModel> {
        return repository.getMovie(id)
    }
}