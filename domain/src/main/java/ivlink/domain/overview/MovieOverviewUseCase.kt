package ivlink.domain.overview

import androidx.lifecycle.LiveData
import ivlink.domain.common.ResultState
import ivlink.domain.models.MovieModel

// Created by IvLink on 3/20/19.

class MovieOverviewUseCase(val repository: MovieOverviewRepository) {

    suspend fun getMovies(page: String): LiveData<ResultState<List<MovieModel>>> {
        return repository.getMovies(page)
    }

    suspend fun fillUpDatabase():ResultState<List<MovieModel>>{
        return repository.fillUpDatabase()
    }

}
