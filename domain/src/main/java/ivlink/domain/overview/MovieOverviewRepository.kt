package ivlink.domain.overview

import androidx.lifecycle.LiveData
import ivlink.domain.common.ResultState
import ivlink.domain.models.MovieModel

interface MovieOverviewRepository {
    suspend fun fillUpDatabase(): ResultState<List<MovieModel>>
    suspend fun getMovies(page: String): LiveData<ResultState<List<MovieModel>>>
}