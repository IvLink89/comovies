package ivlink.domain.common

import java.lang.Exception

sealed class ResultState<T> {

    data class Success<T>(val data: T) : ResultState<T>()

    data class Error<T>(val exception: Exception) : ResultState<T>()
}
