package ivlink.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "movies")
data class MovieEntity(
    @PrimaryKey
    val id: Long = 0,
    val title: String,
    @SerializedName("vote_average")
    val voteAverage: Float = 0F,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    val adult: Boolean,
    val overview: String?,
    @SerializedName("release_date")
    val releaseDate: Calendar
)