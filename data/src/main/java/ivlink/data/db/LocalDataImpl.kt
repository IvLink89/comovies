package ivlink.data.db

import androidx.lifecycle.LiveData
import ivlink.data.repository.LocalData

// Created by IvLink on 3/22/19.
class LocalDataImpl(private val movieDao: MovieDao) : LocalData {

    override suspend fun getMovies(): List<MovieEntity> {
        return movieDao.getMovies()
    }

    override suspend fun insertMovies(movies: List<MovieEntity>) {
        movieDao.insertMovies(movies)
    }

    override suspend fun deleteMovies() {
        movieDao.deleteMovies()
    }

    override fun getMovie(id: Long): LiveData<MovieEntity> {
        return movieDao.getMovie(id)
    }
}