package ivlink.data.utils

import android.os.Environment

const val DATABASE_NAME = "movies_db"
const val MAX_CACHE = 10
const val CACHE_LIFE_IN_MINUTES = 2L
const val DATABASE_UPDATE_IN_MINUTES = 5L
val FILE_CACHE_PATH = Environment.DIRECTORY_DOCUMENTS + "/.comovies_cache"