package ivlink.data.mapper

import ivlink.data.db.MovieEntity
import ivlink.data.models.MovieRemote
import ivlink.data.utils.toCalendar
import ivlink.domain.models.MovieModel

fun MovieEntity.mapToModel(): MovieModel {
    return MovieModel(id, title, voteAverage, posterPath, backdropPath, adult, overview, releaseDate)
}

fun MovieRemote.mapToModel(): MovieModel {
    return MovieModel(id, title, voteAverage, posterPath, backdropPath, adult, overview, releaseDate.toCalendar())
}

fun MovieRemote.mapToEntity(): MovieEntity {
    return MovieEntity(id, title, voteAverage, posterPath, backdropPath, adult, overview, releaseDate.toCalendar())
}

fun MovieModel.mapToEntity(): MovieEntity {
    return MovieEntity(id, title, voteAverage, posterPath, backgroundPath, adult, overview, releaseDate)
}

fun MovieModel.mapToRemote(): MovieRemote {
    return MovieRemote(id, title, voteAverage, posterPath, backgroundPath, adult, overview, releaseDate.time)
}