package ivlink.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import ivlink.data.mapper.mapToModel
import ivlink.domain.details.MovieDetailsRepository
import ivlink.domain.models.MovieModel

// Created by IvLink on 3/23/19.
class MovieDetailsRepoImpl(
    private val localData: LocalData
) : MovieDetailsRepository {

    override fun getMovie(id: Long): LiveData<MovieModel> {
        val data = localData.getMovie(id)
        return Transformations.map(data) {
            it.mapToModel()
        }
    }

}