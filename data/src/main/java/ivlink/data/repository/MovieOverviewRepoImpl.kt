package ivlink.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ivlink.data.mapper.mapToEntity
import ivlink.data.mapper.mapToModel
import ivlink.domain.common.ResultState
import ivlink.domain.models.MovieModel
import ivlink.domain.overview.MovieOverviewRepository

// Created by IvLink on 3/21/19.
class MovieOverviewRepoImpl(
    val context: Context,
    private val localData: LocalData,
    private val remoteData: RemoteData
) : MovieOverviewRepository {

    override suspend fun fillUpDatabase(): ResultState<List<MovieModel>> {
        val movies = remoteData.getMoviesAsync("1").await()
        val list = movies.map {
            it.mapToModel()
        }
        return ResultState.Success(list)
    }

    override suspend fun getMovies(page: String): LiveData<ResultState<List<MovieModel>>> {
        val movies = try {
            remoteData.getMoviesAsync(page).await()
        } catch (exception: Exception) {
            val tempMovies = try {
                localData.getMovies()
            } catch (exceptionDb: Exception) {
                return MutableLiveData(ResultState.Error(exception))
            }
            if (tempMovies.isEmpty()) {
                return MutableLiveData(ResultState.Error(exception))
            }
            return MutableLiveData(ResultState.Success(tempMovies.map {
                it.mapToModel()
            }))
        }
        val listToDb = movies.map {
            it.mapToEntity()
        }
        val listToView = movies.map {
            it.mapToModel()
        }
        localData.insertMovies(listToDb)
        return MutableLiveData(ResultState.Success(listToView))
    }

}