package ivlink.data.repository

import androidx.lifecycle.LiveData
import ivlink.data.db.MovieEntity

// Created by IvLink on 3/21/19.
interface LocalData {

    suspend fun getMovies() : List<MovieEntity>
    suspend fun insertMovies(movies: List<MovieEntity>)
    suspend fun deleteMovies()
    fun getMovie(id : Long) : LiveData<MovieEntity>
}