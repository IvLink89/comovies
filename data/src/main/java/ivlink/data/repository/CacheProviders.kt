package ivlink.data.repository

import com.epam.coroutinecache.annotations.Expirable
import com.epam.coroutinecache.annotations.LifeTime
import com.epam.coroutinecache.annotations.ProviderKey
import com.epam.coroutinecache.annotations.UseIfExpired
import ivlink.data.models.MovieRemote
import ivlink.data.utils.CACHE_LIFE_IN_MINUTES
import kotlinx.coroutines.Deferred
import java.util.concurrent.TimeUnit

// Created by IvLink on 3/22/19.
interface CacheProviders {
    @ProviderKey("MovieKey")
    @LifeTime(value = CACHE_LIFE_IN_MINUTES, unit = TimeUnit.MINUTES)
    @Expirable
    @UseIfExpired
    fun getMoviesAsync(data: Deferred<List<MovieRemote>>): Deferred<List<MovieRemote>>

}