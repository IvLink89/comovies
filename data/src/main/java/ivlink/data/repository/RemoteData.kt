package ivlink.data.repository

import ivlink.data.models.MovieRemote
import kotlinx.coroutines.Deferred

// Created by IvLink on 3/21/19.
interface RemoteData {
    fun getMoviesAsync(page: String): Deferred<List<MovieRemote>>
}