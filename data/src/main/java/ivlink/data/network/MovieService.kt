package ivlink.data.network

import ivlink.data.models.MovieRemote
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {

    @GET(
        "discover/movie?certification_country=US&adult=false" +
                "&with_original_language=en&sort_by=primary_release_date.desc"
    )
    fun getMoviesAsync(
        @Query("primary_release_date.lte") releaseDate: String,
        @Query("page") page: String
    ): Deferred<List<MovieRemote>>
}