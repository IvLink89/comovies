package ivlink.data.network

import ivlink.data.models.MovieRemote
import ivlink.data.repository.RemoteData
import ivlink.data.utils.getApiFormat
import kotlinx.coroutines.Deferred
import java.util.*

// Created by IvLink on 3/21/19.
class RemoteDataImpl(private val movieService: MovieService) : RemoteData {

    override fun getMoviesAsync(page: String): Deferred<List<MovieRemote>> {
        return movieService.getMoviesAsync(Calendar.getInstance().getApiFormat(), page)
    }
}