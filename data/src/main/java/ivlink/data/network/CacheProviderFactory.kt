package ivlink.data.network

import android.content.Context
import com.epam.coroutinecache.api.CacheParams
import com.epam.coroutinecache.api.CoroutinesCache
import com.epam.coroutinecache.mappers.GsonMapper
import ivlink.data.repository.CacheProviders
import ivlink.data.utils.MAX_CACHE

// Created by IvLink on 3/22/19.
object CacheProviderFactory {

    @JvmStatic
    fun getCacheProvider(context: Context): CacheProviders {
        val coroutinesCache = CoroutinesCache(
            CacheParams(
                MAX_CACHE,
                GsonMapper(),
                context.cacheDir
            )
        )
        return coroutinesCache.using(CacheProviders::class.java)
    }
}