package ivlink.data.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieRemote(
    val id: Long = 0,
    val title: String,
    @SerializedName("vote_average")
    val voteAverage: Float = 0F,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    val adult: Boolean,
    val overview: String?,
    @SerializedName("release_date")
    val releaseDate: Date
)